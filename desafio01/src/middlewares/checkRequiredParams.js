module.exports = (req, res, next) => {
  const { nome } = req.query;


  /*
  Como a rota /check utiliza o redirect, e apenas o nome deveria ser
  enviado por GET, se eu ter que validar a data de nascimento
  nunca iria realmente chegar ao final do processo.

  Deixei aqui esta mensagem caso alguma coisa mude em minha avaliação deste
  desafio.
  */

  if (!nome) res.redirect('/');
  else next();
};
