const express = require('express');
const bodyParser = require('body-parser');
const nunjucks = require('nunjucks');
const path = require('path');
const moment = require('moment');

const checkRequiredParams = require('./src/middlewares/checkRequiredParams');

const app = express();

app.use(bodyParser.urlencoded({
  extended: false,
}));

nunjucks.configure('views', {
  autoescape: true,
  express: app,
});

app.set('view engine', 'njk');
app.set('views', path.join(__dirname, 'views'));

app.get('/', (_req, res) => {
  res.render('main');
});

app.post('/check', (req, res) => {
  // validar se a pessoa é maior ou menor de idade
  // e redirecionar para a view correta

  const { dataNascimento, nome } = req.body;

  const idade = moment().diff(moment(dataNascimento, 'DD/MM/YYYY'), 'years');

  if (idade < 18) res.redirect(`/minor?nome=${nome}`);

  res.redirect(`/major?nome=${nome}`);
});

app.get('/major', checkRequiredParams, (req, res) => {
  const { nome } = req.query;
  res.render('major', { nome });
});

app.get('/minor', checkRequiredParams, (req, res) => {
  const { nome } = req.query;
  res.render('minor', { nome });
});

app.listen(3000);

/* eslint-disable-next-line */
console.info('Node server running on 3000 port');
